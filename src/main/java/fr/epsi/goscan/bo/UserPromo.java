package fr.epsi.goscan.bo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "USER_PROMO")
public class UserPromo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "promo_id", nullable = false)
    private Promo promo;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    private boolean used = false;

    @Nullable
    @Temporal(TemporalType.DATE)
    private Date used_date;

    public UserPromo() {}

    public UserPromo(Promo promo, User user) {
        this.promo = promo;
        this.user = user;
    }

    public Promo getPromo() {
        return promo;
    }

    public void setPromo(Promo promo) {
        this.promo = promo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public Date getUsed_date() {
        return used_date;
    }

    public void setUsed_date(Date used_date) {
        this.used_date = used_date;
    }

    @Override
    public String toString() {
        return "UserPromo{" +
                "promo=" + promo +
                ", user=" + user +
                ", used=" + used +
                '}';
    }
}
