package fr.epsi.goscan;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoscanApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoscanApplication.class, args);
    }
}
