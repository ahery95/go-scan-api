package fr.epsi.goscan.data;

import fr.epsi.goscan.bo.UserPromo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RepositoryRestResource(path = "scans",collectionResourceRel = "scans")
public interface IUserPromoRepository extends CrudRepository<UserPromo, Long> {
    List<UserPromo> findByUsedIsNot(boolean bool);

    @Query(value = "SELECT count(*) FROM user_promo up join promos p on up.promo_id = p.id join users u on up.user_id = u.id where p.qrcode = ?1 and u.id = ?2", nativeQuery = true)
    Integer alreadyScanned(String qrCode, Long id);

    @Modifying
    @Transactional
    @Query(value = "UPDATE user_promo SET used=true , used_date= CURRENT_TIMESTAMP WHERE promo_id = ?1 and user_id = ?2", nativeQuery = true)
    void useScan(Long promo_id, Long user_id);

    @Query(value = "select used_date, count(*) FROM user_promo where used = true group by used_date", nativeQuery = true)
    List<Object> nbScanUsedPerDate();

    @Query(value = "select count(*), p.code from user_promo up join promos p on up.promo_id = p.id group by p.id limit 5", nativeQuery = true)
    List<Object> mostCodeUsed();

}
