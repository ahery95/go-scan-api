package fr.epsi.goscan.data;

import fr.epsi.goscan.bo.Promo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path = "promos",collectionResourceRel = "promos")
public interface IPromoRepository extends CrudRepository<Promo, Long> {

    Promo findByQrcode(String qrcode);

    List<Promo> findByReusableIsTrue();

    //Route qui permet de récupérer tous les codes promos encore valide
    @Query(value = "SELECT * FROM promos where validity_date > CURRENT_TIMESTAMP", nativeQuery = true)
    List<Object> findUpPromos();

    @Query(value = "select count(*) from promos where reusable = false", nativeQuery = true)
    Long nbUniqueCode();

    @Query(value = "select count(*) from user_promo up join promos p on up.promo_id = p.id where p.reusable = false and up.used = true", nativeQuery = true)
    Long nbUniqueCodeUsed();

}
