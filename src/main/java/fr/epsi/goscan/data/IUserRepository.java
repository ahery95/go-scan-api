package fr.epsi.goscan.data;

import fr.epsi.goscan.bo.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IUserRepository extends CrudRepository<User, Long> {
    User findByEmailAndPassword(String email, String password);

    //Route pour simplifier l'accès aux scans d'un utilisateur
    @Query(value = "SELECT p.id, p.code,  p.description, p.qrcode, p.reusable, p.validity_date, up.used from promos p join user_promo up on p.id = up.promo_id join users u on up.user_id = u.id where u.id = ?1", nativeQuery = true)
    List<Object> findScansByUser(Long idUser);

}
