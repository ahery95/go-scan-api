package fr.epsi.goscan.controller;

import fr.epsi.goscan.bo.Promo;
import fr.epsi.goscan.bo.User;
import fr.epsi.goscan.bo.UserPromo;
import fr.epsi.goscan.data.IPromoRepository;
import fr.epsi.goscan.data.IUserPromoRepository;
import fr.epsi.goscan.data.IUserRepository;
import fr.epsi.goscan.pojo.JsonUserPromo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/scans")
public class UserPromoController {

    @Autowired
    private IPromoRepository PromoRepository;

    @Autowired
    private IUserRepository UserRepository;

    @Autowired
    private IUserPromoRepository UserPromoRepository;

    //Route qui permet d'associer un utilisateur à un code promo
    @PostMapping()
    public UserPromo addScan(@Valid @RequestBody JsonUserPromo json) {
        Optional<Promo> p = Optional.ofNullable(PromoRepository.findByQrcode(json.getQrcode()));
        Optional<User> u = UserRepository.findById(json.getUser_id());

        UserPromo userPromo = new UserPromo(p.get(), u.get());

        if(UserPromoRepository.alreadyScanned(json.getQrcode(), json.getUser_id()) == 1){
            return userPromo;
        }else{
            UserPromoRepository.save(userPromo);
            return userPromo;
        }
    }

    //Routes permettant de récupérer tous les scans
    @GetMapping()
    public List<UserPromo> GetUserPromo() {
        return (List<UserPromo>) UserPromoRepository.findAll();
    }

    //Route permettant de récupérer tous les scans utilisés
    @GetMapping(value = "/used")
    public List<UserPromo> GetUserPromoUsed() {
        return (List<UserPromo>) UserPromoRepository.findByUsedIsNot(false);
    }

    //Route permettant d'utiliser une promo'
    @PutMapping(value = "/use/{promoId}")
    public String usePromo(@PathVariable Long promoId, @RequestBody JsonUserPromo user_id){
        UserPromoRepository.useScan(promoId, user_id.getUser_id());
        return "Ok";
    }

    //Nombre de scans utilisés par date
    @GetMapping(value = "/nbScanUsedPerDate")
    public List<Object> nbScanUsedPerDate(){
        return UserPromoRepository.nbScanUsedPerDate();
    }

    //Promos les plus utilisées
    @GetMapping(value = "/mostCodeUsed")
    public List<Object> mostCodeUsed(){
        return UserPromoRepository.mostCodeUsed();
    }

}
