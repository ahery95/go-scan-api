package fr.epsi.goscan.controller;

import fr.epsi.goscan.bo.User;
import fr.epsi.goscan.data.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private IUserRepository UserRepository;

    //Route qui permet de récupérer toutes les promos d'un utilisateur
    @GetMapping("/{userId}/scans")
    public List<Object> getUserScans(@PathVariable Long userId){
        return UserRepository.findScansByUser(userId);
    }

    //Route qui permet à un utilisateur de s'authentifier
    @PostMapping("/login")
    public ResponseEntity<User> login(@Valid @RequestBody User user) {
        User u = UserRepository.findByEmailAndPassword(user.getEmail(), user.getPassword());
        if(u == null){
            User response = new User();
            response.setId(0L);
            return ResponseEntity.ok(response);
        }else{
            return ResponseEntity.ok(u);
        }
    }

}
