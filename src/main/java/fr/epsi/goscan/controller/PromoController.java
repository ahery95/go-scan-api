package fr.epsi.goscan.controller;

import fr.epsi.goscan.bo.Promo;
import fr.epsi.goscan.data.IPromoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/promos")
public class PromoController {

    @Autowired
    private IPromoRepository PromoRepository;

    //Route permettant de récupérer toutes les promos encore valides
    @GetMapping("/up")
    public List<Object> getUserScans(){
        return PromoRepository.findUpPromos();
    }

    //Route pemettant de récupérer toutes les promos réutilisables
    @GetMapping("/reusable")
    public List<Promo> getPromoReusable() { return  PromoRepository.findByReusableIsTrue(); }

    //Route permettant de récupérer toutes les promos non réutilisables
    @GetMapping("/nbUniqueCode")
    public Long nbUniqueCode() { return  PromoRepository.nbUniqueCode(); }

    //Route permettant de récupérer toutes les promos non réutilisables utilisées
    @GetMapping("/nbUniqueCodeUsed")
    public Long nbUniqueCodeUsed() { return  PromoRepository.nbUniqueCodeUsed(); }
}
