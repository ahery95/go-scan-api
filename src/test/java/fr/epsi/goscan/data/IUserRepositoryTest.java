package fr.epsi.goscan.data;

import fr.epsi.goscan.bo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@SpringBootTest
class IUserRepositoryTest {

    @Autowired
    private IUserRepository dao;

    @Test
    public void testSave() throws NoSuchAlgorithmException {

        User user = new User();
        user.setEmail("herya@gmail.com");
        String password = "epsi";
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] messageDigest = md.digest(password.getBytes());
        user.setPassword(messageDigest.toString());
        user.setAdmin(true);


        dao.save(user);
    }
}
