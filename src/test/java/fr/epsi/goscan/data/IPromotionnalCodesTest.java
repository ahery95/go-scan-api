package fr.epsi.goscan.data;


import fr.epsi.goscan.bo.Promo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest
public class IPromotionnalCodesTest {
    @Autowired
    private IPromoRepository dao;

    @Test
    public void promoSave(){
        Date date = new Date();
        Promo promo = new Promo();
        promo.setCode("Asso2020");
        promo.setDescription("Remise de 30% sur le deuxieme pull acheté");
        promo.setQrcode("fsjflkd1234445DS");
        promo.setReusable(true);
        promo.setValidity_date(date);
        dao.save(promo);
    }
}
