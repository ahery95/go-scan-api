package fr.epsi.goscan.controller;
import fr.epsi.goscan.data.IUserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {UserController.class, IUserRepository.class})
public class UserControllerTest {

    @InjectMocks
    private UserController testingObject;

    @Mock
    private IUserRepository userRepository;

    private MockMvc mvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this); //Initialisation de la classe à tester
        mvc = MockMvcBuilders.standaloneSetup(testingObject).build();
    }


    //Test qui permet de voir si en cas de bad params ou de mauvaises valeurs d'entrées la route nous retourne bien un utilisateur vide
    @Test
    public void bad_login_informations() throws Throwable {
        mvc.perform(post("/users/login").content("{\"id\": 4098305}").characterEncoding("utf-8").contentType(APPLICATION_JSON).accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(0));
    }

    //Test qui permet de savoir si la route est up
    @Test
    public void check_users_scan() throws Throwable {
        mvc.perform(get("/users/1/scans")).andExpect(status().isOk());
    }
}
