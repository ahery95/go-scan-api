package fr.epsi.goscan.controller;

import fr.epsi.goscan.data.IPromoRepository;
import fr.epsi.goscan.data.IUserPromoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {UserPromoController.class, IUserPromoRepository.class})
public class UserPromoControllerTest {

    @InjectMocks
    private UserPromoController testingObject;

    @Mock
    private IUserPromoRepository userPromoRepository;

    private MockMvc mvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this); //Initialisation de la classe à tester
        mvc = MockMvcBuilders.standaloneSetup(testingObject).build();
    }

    //Test qui permet de savoir si la route est up
    @Test
    public void check_user_promo() throws Throwable {
        mvc.perform(get("/scans")).andExpect(status().isOk());
    }

    //Test qui permet de savoir si la route est up
    @Test
    public void check_scans_used() throws Throwable {
        mvc.perform(get("/scans/used")).andExpect(status().isOk());
    }

    //Test qui permet de savoir si la route est up
    @Test
    public void check_scans_nbScanUsedPerDate() throws Throwable {
        mvc.perform(get("/scans/nbScanUsedPerDate")).andExpect(status().isOk());
    }

    //Test qui permet de savoir si la route est up
    @Test
    public void check_scans_mostCodeUsed() throws Throwable {
        mvc.perform(get("/scans/mostCodeUsed")).andExpect(status().isOk());
    }

    //Test qui permet de savoir que l'erreur renvoyée en cas de mauvais paramètres est bien une bad request
    @Test
    public void bad_params_use() throws Throwable {
        mvc.perform(put("/scans/use/thisIsAString")).andExpect(status().isBadRequest());
    }

}
