package fr.epsi.goscan.controller;

import fr.epsi.goscan.data.IPromoRepository;
import fr.epsi.goscan.data.IUserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {PromoController.class, IPromoRepository.class})
public class PromoControllerTest {

    @InjectMocks
    private PromoController testingObject;

    @Mock
    private IPromoRepository promoRepository;

    private MockMvc mvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this); //Initialisation de la classe à tester
        mvc = MockMvcBuilders.standaloneSetup(testingObject).build();
    }

    //Test qui permet de savoir si la route est up
    @Test
    public void check_promos_up() throws Throwable {
        mvc.perform(get("/promos/up")).andExpect(status().isOk());
    }

    //Test qui permet de savoir si la route est up
    @Test
    public void check_promos_reusable() throws Throwable {
        mvc.perform(get("/promos/reusable")).andExpect(status().isOk());
    }

    //Test qui permet de savoir si la route est up
    @Test
    public void check_promos_nbUniqueCode() throws Throwable {
        mvc.perform(get("/promos/nbUniqueCode")).andExpect(status().isOk());
    }

    //Test qui permet de savoir si la route est up
    @Test
    public void check_promos_nbUniqueCodeUsed() throws Throwable {
        mvc.perform(get("/promos/nbUniqueCodeUsed")).andExpect(status().isOk());
    }
}
